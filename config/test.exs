use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :jerm, Jerm.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :jerm, Jerm.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "jerm_test",
  hostname: "localhost",
  port:     5432,
  pool: Ecto.Adapters.SQL.Sandbox

config :hound,
  driver: "selenium",
  browser: "chrome",
  host: "http://127.0.0.1",
  port: 4444
