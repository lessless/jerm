#!/bin/bash

# This script is a part of CI process and has two functions "up" and "down"
# function "up" spin up docker images with unique namespace and port, given that ${EXECUTOR_NUMBER} is set
# function "down"  deletes those containers

APP_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )
DOCKER_NETWORK="jerm_build_dev_${BUILD_NUMBER}"

export PG_PORT=$((${PG_BASE_PORT}+${EXECUTOR_NUMBER}))
export SL_PORT=$((${SL_BASE_PORT}+${EXECUTOR_NUMBER}))
export TEST_PORT=$((${TEST_BASE_PORT}+${EXECUTOR_NUMBER}))

 function up {
  sed -i "s|${TEST_BASE_PORT}|${TEST_PORT}|" "${APP_ROOT}/config/test.exs"

  for conf in $(ls ${APP_ROOT}/config/{dev,test}.exs); do
    sed -i "s|5432|${PG_PORT}|" $conf
    sed -i "s|4444|${SL_PORT}|" $conf
  done

  cd ci && docker-compose -p ${DOCKER_NETWORK} up --build -d
}


function down {
  cd ci && docker-compose -p ${DOCKER_NETWORK} down --volumes
}

if  [ "$1" == 'up' ]; then
  up
  exit $?
elif [ "$1" == 'down' ]; then
  down
  exit $?
else
  echo 'Specify an action please: "up" or "down"'
  exit 1
fi
