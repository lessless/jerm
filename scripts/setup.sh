#!/bin/bash

APP_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )

mix local.hex --force
mix local.rebar --force

mix deps.get --quiet
npm i  --silent
mix ecto.create > /dev/null
mix ecto.migrate > /dev/null
