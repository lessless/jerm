defmodule Jerm.Feature.IndexPageTitleTest do
  use ExUnit.Case
  use Hound.Helpers
  hound_session

    test "loading the home page" do
      navigate_to "http://172.17.0.1:4001"
      assert page_source =~ "Hello Jerm!"
    end
  end
