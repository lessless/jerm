defmodule Jerm.PageController do
  use Jerm.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
